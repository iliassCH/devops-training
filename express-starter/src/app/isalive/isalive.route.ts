import { Request, Response, Router } from 'express';
import { asyncRoute } from '../common/utils';

export interface IsAliveResp {
  isAlive: boolean;
}

async function isAlive(req: Request, res: Response) {
  // Fake async fetch
  const resBody: IsAliveResp = await Promise.resolve({ isAlive: true });
  res.json(resBody);
}

export function isAliveRouter(): Router {
  const router = Router();
  router.get('/', asyncRoute(isAlive));
  router.post('/', asyncRoute(isAlive));
  return router;
}
