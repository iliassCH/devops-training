import { NOT_ACCEPTABLE } from 'http-status-codes';
import { testGet } from '../common/test-utils';
import { MyFeatureResp } from './feat1.routes';

describe('/feat', () => {
  it('should echo the query params', async () => {
    const { body }: { body: MyFeatureResp } = await testGet('/feat?foo=bar');
    expect(body).toEqual({ asyncData: 'here', foo: 'bar' });
  });

  it('should not accept swearing bad words', async () => {
    const { body }: { body: MyFeatureResp } = await testGet(
      '/feat?fuck',
      { status: NOT_ACCEPTABLE });
    expect(body).toEqual({ status: NOT_ACCEPTABLE, error: 'DONT_SWEAR', name: 'Error' });
  });
});
