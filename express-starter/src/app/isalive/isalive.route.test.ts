import { testGet } from '../common/test-utils';
import { IsAliveResp } from './isalive.route';

describe('/ (is alive)', () => {
  it('should return ok', async () => {
    const { body: { isAlive } }: { body: IsAliveResp } = await testGet('/');
    expect(isAlive).toEqual(true);
  });
});
