import consoleStamp = require('console-stamp');
import express = require('express');
import { Express } from 'express';
import * as logger from 'morgan';
import { join } from 'path';
import { initErrorRoutes } from './common/error.routes';
import { isAliveRouter } from './isalive/isalive.route';
import { myFeatureRouter } from './myfeature/feat1.routes';

// Practice to follow: don't execute any code at file root level. Always wrap in functions that you
// call when you need it. It will prevent issues with import order (the app behavior should not
// rely on import order)

export function initApp(silent?: boolean): Express {
  const app = express();

  consoleStamp(console, {});
  if (!silent) {
    app.use(logger('dev'));
  }
  app.use(express.json());
  // app.use(express.urlencoded({ extended: false }));
  app.use(express.static(join(__dirname, '../public')));

  app.use('/', isAliveRouter());
  app.use('/', myFeatureRouter());
  initErrorRoutes(app);
  return app;
}
