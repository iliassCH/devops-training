import { Application, NextFunction, Request, Response } from 'express';
import { INTERNAL_SERVER_ERROR } from 'http-status-codes';

export interface ErrorResp {
  status: number;
  error: string;
  code?: number;
  name?: string;
}

export function initErrorRoutes(app: Application): void {
  app.use((e: any, req: Request, res: Response, next: NextFunction) => {
    let err = e; // Because we will reassign
    // Axios response errors
    if (err && err.response && err.response.data) {
      err = err.response.data;
    }
    const status = (err && (err.status || err.statusCode)) || INTERNAL_SERVER_ERROR;
    const error = err && (err.message || (err.error && errorToStr(err.error)) || errorToStr(err));
    const code = err && err.code;
    const resBody: ErrorResp = { status, error, code, name: err.name };
    if (status >= 500) {
      console.error(err);
      console.error(resBody);
    }
    res.status(status).json(resBody);
  });
}

function errorToStr(error: any): string {
  return typeof error === 'string' ? error : JSON.stringify(error);
}
