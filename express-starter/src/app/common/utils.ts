import { NextFunction, Request, Response } from 'express';

/**
 * Route wrapper, helper to automatically catch Promise errors and pass it to the error handler.
 * @param fn
 */
export function asyncRoute(fn: (req: Request, res: Response, next: NextFunction) => Promise<any>) {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await fn(req, res, next);
    } catch (err) {
      next(err);
    }
  };
}

export class ErrorWithStatus extends Error {
  constructor(message: string, public status: number) {
    super(message);
  }
}

export function errWithStatus(message: string, status: number): ErrorWithStatus {
  return new ErrorWithStatus(message, status);
}
