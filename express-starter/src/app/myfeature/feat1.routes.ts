import { Request, Response, Router } from 'express';
import { NOT_ACCEPTABLE } from 'http-status-codes';
import { asyncRoute, errWithStatus } from '../common/utils';

export interface MyFeatureReq {
  [key: string]: string;
}

export interface MyFeatureResp {
  [key: string]: string;
}

async function featRoute(req: Request, res: Response): Promise<void> {
  const query: MyFeatureReq = req.query;
  // Reject specific param values (we could use other tools like express-validator)
  if (query.hasOwnProperty('fuck')) {
    throw errWithStatus('DONT_SWEAR', NOT_ACCEPTABLE);
  }
  // Simulate fetching async data with promise. Errors are handled by `asyncRoute` wrapper.
  const dummyData = await Promise.resolve({ asyncData: 'here' });
  // Response is request query params echo plus the async data we fetched.
  // Those 2 objects are merged into one, giving priority to query attributes.
  const resBody: MyFeatureResp = { ...dummyData, ...query };
  res.json(resBody);
}

export function myFeatureRouter(): Router {
  const router = Router();
  router.get('/feat', asyncRoute(featRoute));
  return router;
}
