import { config } from 'dotenv';

// Environment variables are loaded from .env and OS
config();

interface Env {
  port: string;
}

// Default values should be non-sensitive production values
// to reduce environment variable issues when deploying.
// sensitive values (e.g. passwords, secrets) should not have default values.
export const env: Env = {
  port: process.env.PORT || '5000',
};

const requiredVariables: (keyof Env)[] = [
  // Add here environment variables that MUST be provided in .env. This is useful
  // to ensure it won't be omitted in new environments like pre-prod/prod.
];

// Guard to prevent app boot if a required environment variable is missing
const missingVariables = requiredVariables.filter((key: keyof Env) => !env[key]);
if (missingVariables.length) {
  throw new Error(`Missing environment variables: ${missingVariables.join(', ')}`);
}
